FREST::RbLoader.defn(
  arg_types:   {
    path:     'path',
  },
  result_type: 'relation',
) do |
  path:,
  **c
|
  result = target.(
    context: context,
    **c
  )

  FREST::Relation.new oftype: FREST::RbLoader.load(path: ['t', **path[1..-1]])
end