require 'rack'
require 'uri'

FREST::RbLoader.defn(
  arg_types:   {
    port:       'integer',
    context:    'context',
  },
  result_type: 'process',
  requires: [
    'loader',
    'converter'
  ]
) do |
  context:,
  loader:,
  converter:,
  port:       8943,
  path:       [],
  **c
|

  app = ->(env) {
    req     = Rack::Request.new(env)
    path    = req.path_info.split('/')

    path = path[1..-1] if path&.first&.chomp == ''
    path = 'root' if path == []
    target, type  = loader.(path: path)

    result, code = converter.(
      loader:   loader,
      target:   target,
      context:  context,
      path:     path,
      to_type:  req.media_type,
      **c
    )
    # headers      = mime_fn.(path: File.join(*path))

    code ||= 200

    if result.respond_to? :each
      [code, {}, result]
    else
      [code, {}, [result]]
    end
  }
  
  Rack::Handler::WEBrick.run app, Port: port
end


def default
  @def ||= File.read 'frest/web/root.html'
  [200, {}, [@def]]
end