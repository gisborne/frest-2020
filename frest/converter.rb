FREST::RbLoader.defn(
  arg_types:   {
    target:   'thing',
    context:  'context',
    path:     'path',
    to_type:  'type'
  },
  result_type: 'thing',
) do |
  path:,
  target:,
  context:,
  to_type:,
  **c
|
  '<H1>Default!</H1>'
end