class FREST::BaseFileLoader
  @@path_cache = {}

  class << self

    def load path:, **c
      if path.is_a? Array
        #TODO move this to root so it's just always true
        path = path[1..] if path&.first == ''
        path = maybe_process_path path: path, **c
        path = path.prepend 'frest' unless path.first == 'frest'
        path = path.join('/')
      end

      load_internal(path: path, **c)
    end

    private

    def load_internal path:, **c
      @@path_cache[path] ||= load_without_cache(file_path: path, **c)
    end

    def maybe_process_path path:, **c
      path
    end
  end
end