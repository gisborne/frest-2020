require 'pathname'
require_relative 'base_file_loader'

class FREST::Loader
  class << self
    loaders_path = Pathname.new(__dir__).join 'loaders'
    FREST_PATH   = Pathname.new(__dir__).parent + 'frest'
    @@loaders     = []

    Dir.each_child loaders_path do |f|
      require Pathname.new(loaders_path).join f
      f_name = f.gsub(/\..*$/, '')
      @@loaders.push Object.const_get('FREST::' + f_name.split('_').map(&:capitalize).join)
    end

    @@loaders.sort_by! { |x| x.priority }

    def load(
      path:
    )

      @@loaders.each do |x|
        real_path = path.is_a?(String) ? path.split('/') : path
        result, result_type = x.load path: real_path, root_path: FREST_PATH
        return result, result_type if result
      end

      return nil
    end
  end
end