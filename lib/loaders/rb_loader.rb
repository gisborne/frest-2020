class FREST::RbLoader < FREST::BaseFileLoader
  @@path_cache = {}

  class << self
    def priority
      0
    end

    def defn **args, &block
      Thread.current[:new_fn] = RubyFn.new **args, &block
    end

    private

    def extension
      'rb'
    end

    def load_without_cache file_path:, **c
      begin
        require file_path
        return Thread.current[:new_fn], 'ruby'
      rescue LoadError => e
        return nil
      end
    end

  end

  class RubyFn
    attr_reader :metadata

    def initialize **metadata, &block
      @metadata = metadata
      @fn       = block
      @requires = {}

      if requires = metadata['requires']
        @requires = requires.map { |req| FREST::RBLoader.load req }
      end
    end

    def call path: ['_'], **c
      @fn.call(
        path: path,
        **@requires,
        **c
      )
    end
  end
end
