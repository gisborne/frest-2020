module FREST
  class DirectoryLoader
    GOOD_EXTENSIONS = %w{ico html png pdf jpg jpeg gif}
      .map { |x| [Regexp.new("\.#{x}$"), x] }.to_h

    class << self
      def priority
        2
      end

      def load path:, root_path:
        p = Pathname.new(root_path)
        p = p.join path&.first if path&.first

        # A +.rb file overrides regular directory handling
        if p.exist?
          handle_subfile(
            p,
            path:      path,
            root_path: root_path
          )
        elsif (plus_file = p.join('+.rb')).exist?
          handle_control_file(
            plus_file,
            path:      path,
            root_path: root_path
          )
        elsif (minus_file = p.join('-.rb')).exist?
          handle_control_file(
            minus_file,
            path:      path,
            root_path: root_path
          )
        else
          nil
        end
      end

      private

      def handle_controlfile(controlfile_path, path:, root_path:)
        fn = RbLoader.load controlfile_path
        fn.(path)
      end

      def handle_subfile(subfilepath, path:, root_path:)
        if type = get_type(subfilepath)
          return File.open(subfilepath, 'rb').read, type
        end
      end

      def get_type subfilepath
        GOOD_EXTENSIONS.each do |k, v|
          return v if k.match? subfilepath.to_s
        end

        return nil
      end
    end
  end
end