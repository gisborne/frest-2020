require 'yaml'
# FREST files are YML files interpreted as a namespace
class FREST::FrestLoader < FREST::BaseFileLoader
  class << self
    def priority
      1
    end

    private

    def load_without_cache file_path:, **c
      return nil if File.directory?(file_path)

      begin
        y = YAML.load File.read(file_path)
      rescue Exception
        return nil
      end

      f = y.keys.first

    end

    def extension
      'frest'
    end
  end
end
