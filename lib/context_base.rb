class FREST::ContextBase
  @parent = null

  def + other_context
    other_context.set_parent self
  end
  
  def set_parent other_context
    @parent = other_context
  end
end