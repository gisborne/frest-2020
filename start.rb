#!/usr/bin/env ruby

# load frest base
ROOT_DIR = __dir__
$:.unshift File.dirname(__FILE__)
module FREST; end

require_relative 'lib/loader.rb'
loader = FREST::Loader.method :load

root_context, _  = loader.(path: 'frest')
frest_web, _     = loader.(path: 'frest/frest_web.rb')
converter, _     = loader.(path: 'frest/converter.rb')
user_context, _  = loader.(path: 'frest/dummy_user_content.rb')

frest_web.(
  context:    root_context,
  converter:  converter,
  loader:     loader
)
